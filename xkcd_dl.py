#! /usr/bin/python3
#Downloads all XKCD comics till


import os,bs4,requests,sys

base_url='https://www.xkcd.com'
ext=''
if os.path.exists('first'):
    first=open('first')
    ext=first.read()
    first.close()
else:
    first=open('first','w')
    first.write('/1/')
    first.close
    ext='/1/'
    
#print('Starting from #')
# ext='/1626'
while ext!='#':
    print('Getting website %s'%(base_url+ext))
    site=requests.get(base_url+ext)
    soup_obj=bs4.BeautifulSoup(site.text,"html.parser")
    first=open('first','w')
    first.write(ext)
    first.close
    ext=soup_obj.select('.comicNav a[rel="next"]')[0].get('href')
        #if len(sys.argv)==2 and sys.argv[1]!='i':    
        #last=open('last','w')
        #last.write(ext)
        #last.close()
    if soup_obj.select('#comic img')==[]:
        continue
    comic_url='https:'+soup_obj.select('#comic img')[0].get('src')
    name=soup_obj.select('#comic img')[0].get('alt')
    if name=='':
        continue
    name=' '.join(name.split('/'))
    if os.path.exists(name)==False:
        site2=requests.get(comic_url)
        image_file=open(name,'wb')
        print('Downloading image \'%s\''%name)
        for chunk in site2.iter_content(100000):
            image_file.write(chunk)
        image_file.close()
    else:
        print('Image already exists')
        
print('UPDATED!')

